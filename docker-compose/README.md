# Northstar Deployment (Docker Compose)

### Configuration

Please install [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).

Run `docker login registry.gitlab.com` and input the username/password we have provided.

### Deploy

There are two environment variables for configuring the deployment:
- `NORTHSTAR_DIR`: the folder where you want to store all Northstar-related files, e.g., `$HOME/northstar`
- `NORTHSTAR_IMAGE_TAG`: the image tag (we will provide this to you)

For example,

```bash
export NORTHSTAR_DIR=$HOME/northstar
mkdir -p $NORTHSTAR_DIR
export NORTHSTAR_IMAGE_TAG=test
```

Then use Docker compose:

```bash
docker-compose up -d
```

Then you should be able to use Northstar at `127.0.0.1:1235` through
your browser.

**Note**: You can also use `deploy.sh` to automate the whole process.

### Create User

You need to create a user to log into Northstar. Note that this following step
only needs to be done once as long as the `NORTHSTAR_DIR` doesn't change. You can add a '-a' flag to make a user an admin. 

```bash
docker container exec laax /bin/bash -c 'montana-cli add-user -u test -p test -y'
```

By doing this you create a user with username and password both as `test`.

### Add datasets

You can upload files through the user interface, but also add them programmatically. This is useful for large files that are too big or slow for browser upload. Note that files need to be in the `NORTHSTAR_DIR` directory. A dataset needs to be associated with a user through `-u <username>`. The following formats are supported:

#### Single CSV file
```bash
docker container exec laax /bin/bash -c 'montana-cli add-csv -u test -n "my dataset name" -f $NORTHSTAR_DIR/output/montana/datasets/test.csv -y'
```

#### Single Parquet file
```bash
docker container exec laax /bin/bash -c 'montana-cli add-parquet -u test -n "my dataset name" -f $NORTHSTAR_DIR/output/montana/datasets/test.parquet -y'
```

#### Collection of CSV files in a folder (will be unioned automatically)
```bash
docker container exec laax /bin/bash -c 'montana-cli add-folder -u test -n "my dataset name" -f $NORTHSTAR_DIR/output/montana/datasets/folder -y'
```

### Refresh datasets

If a file or folder dataset has changed you can instruct the system to reload it with:

```bash
docker container exec laax /bin/bash -c 'montana-cli refresh-dataset -f $NORTHSTAR_DIR/output/montana/datasets/folder_or_file -y'
```

### Kill Instance

```bash
docker-compose kill
```

### Metrics

Stats are available to view on Grafana at `127.0.0.1:3000`, with default username / password both `admin` and `bello`. The Prometheus datasource is automatically imported and can be viewed in Explore. You can now also make queries and dashboards using the Prometheus metrics. 


### LDAP
To enable ldap login, add `ldap` to `LOGIN_OPTIONS` and point `LDAP_CONFIG_PATH` to a JSON file like the one shown below:

```
{
    "server": {
        "url": "ldaps://ldaps.company.corp:636",
        "bindDN": "cn=admin,dc=example,dc=com",
        "bindCredentials": "test",
        "searchBase": "ou=users,dc=example,dc=com",
        "searchFilter": "(sAMAccountName={{username}})",
        "tlsOptions": {
            "ca": ["/path/to/certificate1", "/path/to/certificate2"]
        }
    },
    "ldapUserIDField": "sAMAccountName",
    "ldapDisplayNameField": "cn",
    "ldapPhotoURLsField": "",
    "ldapEmailsField": "userPrincipalName"
}
```

Ensure that the file is visible in the container. For example, if your file is named `ldap.json`, you could place it in the input volume (yielding `/input/ldap.json`) and set `LDAP_CONFIG_PATH=/input/ldap.json`.

`url` is the URL to the LDAP server. The standard port for LDAP is 389, while for LDAPS (often used with Active Directory), it is 636. However, your server may use an alternative port, e.g. 3269.

`bindDN` and `bindCredentials` should be the Distinguished Name (DN) and password to some generic, non-user account (in this example, the corresponding user is `admin` with password `test`.) The bind serves to establish an identity to the LDAP server that allows us to conduct operations on the LDAP tree, such as searching for a user's information. 

`searchBase` is the base entry of the tree to start the search for the user. 

`searchFilter` determines the criteria to match the given username to the user in the LDAP database, using standard LDAP search filter syntax.

`tlsOptions` specifies various options for enabling TLS support, e.g. when using LDAPS with Active Directory. In the `ca` field, add an array containing the paths to any required certificates. This can be necessary when using an Enterprise CA, for example. Other options may be specified here, but are likely unnecessary. Certificates must be in PEM format (many file extensions cover the PEM format.) As with the configuration file, ensure that any specified certificate files are visible in the container, e.g. in `/input/cert.pem`.

