# check env vars
if [[ -z "${NORTHSTAR_DIR}" ]]; then
    echo "Environment variable NORTHSTAR_DIR is not set"
    exit 1
fi
if [[ -z "${NORTHSTAR_IMAGE_TAG}" ]]; then
    echo "Environment variable NORTHSTAR_IMAGE_TAG is not set"
    exit 1
fi
if [[ -z "${DOCKER_COMPOSE_FILE}" ]]; then
    DOCKER_COMPOSE_FILE=docker-compose.yaml
fi

echo "DOCKER_COMPOSE_FILE: ${DOCKER_COMPOSE_FILE}"
echo "NORTHSTAR_DIR: ${NORTHSTAR_DIR}"
echo "NORTHSTAR_IMAGE_TAG: ${NORTHSTAR_IMAGE_TAG}"
echo "OSTYPE: $OSTYPE"
echo "DOCKER_COMPOSE_FILE: $DOCKER_COMPOSE_FILE"

# pull images
echo "Pulling images..."
docker pull registry.gitlab.com/einblick/northstar-release/davos:${NORTHSTAR_IMAGE_TAG}
docker pull registry.gitlab.com/einblick/northstar-release/montana:${NORTHSTAR_IMAGE_TAG}

# docker-compose
echo "Starting up docker containers..."
docker-compose -f $DOCKER_COMPOSE_FILE up -d
