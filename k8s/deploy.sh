set -e

# arguments
if [[ -z "${NORTHSTAR_IMAGE_TAG}" ]]; then
    echo "Environment variable NORTHSTAR_IMAGE_TAG is not set"
    exit 1
fi
export SERVICE_YAML_FILE=service.yaml
if [[ -z "${NORTHSTAR_IP}" ]]; then
	export SERVICE_YAML_FILE=service_without_ip.yaml
fi
echo "NORTHSTAR_IMAGE_TAG: ${NORTHSTAR_IMAGE_TAG}"
echo "NORTHSTAR_IP: ${NORTHSTAR_IP}"

# conf
export MONGODB_USER_NAME=${MONGODB_USER_NAME:-root}
export MONGODB_PASSWORD=${MONGODB_PASSWORD:-c10b7bfd980dd736f1ace2455417bfe0}
export MONGODB_HOST=${MONGODB_HOST:-mongodb://127.0.0.1:27017}
export MONGODB_DATABASE=${MONGODB_DATABASE:-northstar}
export GOOGLE_CLIENT_ID=${GOOGLE_CLIENT_ID:-N/A}
export GOOGLE_CLIENT_SECRET=${GOOGLE_CLIENT_SECRET:-N/A}
export GOOGLE_CALLBACK_URL=${GOOGLE_CALLBACK_URL:-N/A}
export GITHUB_CLIENT_ID=${GITHUB_CLIENT_ID:-N/A}
export GITHUB_CLIENT_SECRET=${GITHUB_CLIENT_SECRET:-N/A}
export GITHUB_CALLBACK_URL=${GITHUB_CALLBACK_URL:-N/A}
export LDAP_CONFIG_PATH=${LDAP_CONFIG_PATH:-N/A}
export COOKIE_SECRET=${COOKIE_SECRET:-N/A}
export NORTHSTAR_SLACK_CHANNEL_TOKEN=${NORTHSTAR_SLACK_CHANNEL_TOKEN:-N/A}
export NORTHSTAR_DEPLOY_ENV=${NORTHSTAR_DEPLOY_ENV:-${NORTHSTAR_IMAGE_TAG}}
export NORTHSTAR_HOSTNAME=${NORTHSTAR_HOSTNAME:-http://release.einblick.ai}
export NORTHSTAR_LOGIN_OPTIONS=${NORTHSTAR_LOGIN_OPTIONS:-"\"[\\\"local\\\"]\""}
export NORTHSTAR_BASIC_AUTH_USERS=${NORTHSTAR_BASIC_AUTH_USERS:-"\"{}\""}
export NORTHSTAR_LIVENESS_PROBE_HEADER_NAME=${NORTHSTAR_LIVENESS_PROBE_HEADER_NAME:-User-Agent}
export NORTHSTAR_LIVENESS_PROBE_HEADER_KEY=${NORTHSTAR_LIVENESS_PROBE_HEADER_KEY:-Dummy}
export NORTHSTAR_LAAX_CPUS=${NORTHSTAR_LAAX_CPUS:-8}
export NORTHSTAR_DAVOS_CPUS=${NORTHSTAR_DAVOS_CPUS:-8}
export NORTHSTAR_LAAX_MEMORY=${NORTHSTAR_LAAX_MEMORY:-16Gi}
export NORTHSTAR_DAVOS_MEMORY=${NORTHSTAR_DAVOS_MEMORY:-48Gi}
export NORTHSTAR_DOCKER_SECRET=${NORTHSTAR_DOCKER_SECRET:-regcred}
export NORTHSTAR_VOLUME_CLAIM=${NORTHSTAR_VOLUME_CLAIM:-northstar-claim}

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# update config
${SCRIPT_DIR}/create_or_update_config.sh

# deploy
echo "Deploying ${NORTHSTAR_DEPLOY_ENV} ..."
envsubst < ${SCRIPT_DIR}/pod.yaml | kubectl delete --ignore-not-found -f -
envsubst < ${SCRIPT_DIR}/pod.yaml | kubectl create -f -
envsubst < ${SCRIPT_DIR}/${SERVICE_YAML_FILE} | kubectl apply -f -
