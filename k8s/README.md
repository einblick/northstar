# Northstar Kubernetes Deployment


### Configuration (Init)

A Northstar Kubernetes deployment requires the following three configurations:

1. Credentials for pulling Docker images
    - Please set the environment variable `NORTHSTAR_DOCKER_SECRET` with the the name of the secret, e.g., regcred
2. Volume claim for storing Northstar data
    - We provide an example VolumeClaim file `volume_claim.yaml` for reference, please create your volume claim accordingly and set the environment variable `NORTHSTAR_VOLUME_CLAIM` with the the volume claim name.
    - We provide an example Volume file `volume.yaml` (which works on GCP) for reference.

You can run `init.sh` for the first step with the corresponding environment variables.
    
### Deploy

There are two environment variables for configuring the deployment:
- `NORTHSTAR_IMAGE_TAG`: the image tag (we will provide this to you)
- `NORTHSTAR_IP`: the IP address for exposing the Northstar instance as a Kubernetes service
- `NORTHSTAR_CONFIG_MAP` (optional): the name of the collection of configuration files for components (e.g., Grafana, Prometheus), it is `config` by default, and you can set this variable if there is a name collision

After setting these environment variables, you can run `deploy.sh` to deploy Northstar.

### Create User

You need to create a user to log into Northstar.
First you need to run the shell in the container.

```bash
kubectl exec -it pod-laax-${NORTHSTAR_IMAGE_TAG} -c montana -- /bin/bash
```

In the container, you can run this command to create a user. You can add a '-a' flag to make an admin user (has some special privileges).

```bash
montana-cli add-user -u test -p test -y
```

By doing this you create a user with username and password both as `test`.

### Download Einblick data (Optional)

Einblick data has some large files that we don't include in the Einblick images, e.g., trained neural networks.
You can follow the following steps to download them in advance or the system will automatically downloads the data (if the network is allowed) when necessary.

First, run the shell in the container.

```bash
kubectl exec -it pod-laax-${NORTHSTAR_IMAGE_TAG} -c davos -- /bin/bash
```

In the container, you can run this command to download Einblick data.

```bash
cd /usr/deps/
pip install gdown
gdown https://drive.google.com/uc?id=1M0-6qNNI2tFT5Mts_f-WB79YG4nUDW5u
unzip einblick_data.zip
mv einblick_data/* /einblick_data/
```
