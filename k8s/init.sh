set -e

# arguments
if [[ -z "${NORTHSTAR_DOCKER_USERNAME}" ]]; then
    echo "Environment variable NORTHSTAR_DOCKER_USERNAME is not set"
    exit 1
fi
if [[ -z "${NORTHSTAR_DOCKER_PASSWORD}" ]]; then
    echo "Environment variable NORTHSTAR_DOCKER_PASSWORD is not set"
    exit 1
fi

# create image secret
export NORTHSTAR_DOCKER_SECRET=${NORTHSTAR_DOCKER_SECRET:-regcred}
kubectl create secret docker-registry ${NORTHSTAR_DOCKER_SECRET} --docker-server=registry.gitlab.com --docker-username=${NORTHSTAR_DOCKER_USERNAME} --docker-password=${NORTHSTAR_DOCKER_PASSWORD}

# TODO: create volume

# TODO: create volume claim
