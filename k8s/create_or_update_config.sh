export NORTHSTAR_CONFIG_MAP=${NORTHSTAR_CONFIG_MAP:config}
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
kubectl create configmap config --from-file ${SCRIPT_DIR}/../docker-compose/conf \
                                --from-file ${SCRIPT_DIR}/../docker-compose/conf/grafana/dashboards \
                                --from-file ${SCRIPT_DIR}/../docker-compose/conf/grafana/datasources \
        -o yaml --dry-run=client | kubectl apply -f -
