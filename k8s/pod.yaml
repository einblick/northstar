# Northstar Pod
apiVersion: v1
kind: Pod
metadata:
  name: pod-laax-$NORTHSTAR_DEPLOY_ENV
  labels:
    evalId: pod-laax-$NORTHSTAR_DEPLOY_ENV
    montana: "yes"
    davos: "yes"

spec:
  restartPolicy: Always
  containers:
  - name: montana
    image: registry.gitlab.com/einblick/northstar-release/montana:$NORTHSTAR_IMAGE_TAG
    imagePullPolicy: Always
    ports:
      - name: server
        containerPort: 1235
    env:
      - name: MONGODB_USER_NAME
        value: $MONGODB_USER_NAME
      - name: MONGODB_PASSWORD
        value: $MONGODB_PASSWORD
      - name: MONGODB_HOST
        value: $MONGODB_HOST
      - name: MONGODB_DATABASE
        value: $MONGODB_DATABASE
      - name: GOOGLE_CLIENT_ID
        value: $GOOGLE_CLIENT_ID
      - name: GOOGLE_CLIENT_SECRET
        value: $GOOGLE_CLIENT_SECRET
      - name: GOOGLE_CALLBACK_URL
        value: $GOOGLE_CALLBACK_URL
      - name: GITHUB_CLIENT_ID
        value: $GITHUB_CLIENT_ID
      - name: GITHUB_CLIENT_SECRET
        value: $GITHUB_CLIENT_SECRET
      - name: GITHUB_CALLBACK_URL
        value: $GITHUB_CALLBACK_URL
      - name: LDAP_CONFIG_PATH
        value: $LDAP_CONFIG_PATH
      - name: LAAX_AUTH_SUCCESS
        value: "/?loggedin"
      - name: LAAX_AUTH_FAILURE
        value: "/?loggedout"
      - name: COOKIE_SECRET
        value: $COOKIE_SECRET
      - name: DAVOS_ENDPOINT
        value: "127.0.0.1:8888"
      - name: NORTHSTAR_SLACK_CHANNEL_TOKEN
        value: $NORTHSTAR_SLACK_CHANNEL_TOKEN
      - name: UI_ENVIRONMENT_FILE
        value: "static/config/env.json"
      - name: DATASET_FILE_STORAGE
        value: "/output/montana"
      - name: LOGO_FILE_STORAGE
        value: "/output/montana"
      - name: HOSTNAME
        value: $NORTHSTAR_HOSTNAME
      - name: LOGIN_OPTIONS
        value: $NORTHSTAR_LOGIN_OPTIONS
      - name: BASIC_AUTH_USERS
        value: $NORTHSTAR_BASIC_AUTH_USERS
      - name: NORTHSTAR_IMAGE_TAG
        value: $NORTHSTAR_IMAGE_TAG
      - name: LOKI_HOST
        value: "127.0.0.1"
      - name: LOKI_PORT
        value: "1515"

    volumeMounts:
      - name: northstar-data
        mountPath: /input
        subPath: input-$NORTHSTAR_DEPLOY_ENV
        readOnly: true
      - name: northstar-data
        mountPath: /output
        subPath: output-$NORTHSTAR_DEPLOY_ENV
        readOnly: false
    livenessProbe:
      httpGet:
        path: /health
        port: 1235
        httpHeaders:
        - name: $NORTHSTAR_LIVENESS_PROBE_HEADER_NAME
          value: $NORTHSTAR_LIVENESS_PROBE_HEADER_KEY
      failureThreshold: 3
      initialDelaySeconds: 10
      periodSeconds: 10
      successThreshold: 1
      timeoutSeconds: 5
    command: ["/bin/sh"]
    args: ["-c", "pm2 start dist/src/server.js -i 1 && pm2 logs"]
    resources:
      requests:
        memory: $NORTHSTAR_LAAX_MEMORY
        cpu: $NORTHSTAR_LAAX_CPUS

  - name: davos
    image: registry.gitlab.com/einblick/northstar-release/davos:$NORTHSTAR_IMAGE_TAG
    imagePullPolicy: Always
    ports:
      - name: server
        containerPort: 8888
    env:
      - name: MONGODB_USER_NAME
        value: $MONGODB_USER_NAME
      - name: MONGODB_PASSWORD
        value: $MONGODB_PASSWORD
      - name: MONGODB_HOST
        value: $MONGODB_HOST
      - name: MONGODB_DATABASE
        value: $MONGODB_DATABASE
      - name: NORTHSTAR_IMAGE_TAG
        value: $NORTHSTAR_IMAGE_TAG
      - name: LOKI_HOST
        value: "127.0.0.1"
      - name: LOKI_PORT
        value: "1514"
    volumeMounts:
      - name: northstar-data
        mountPath: /input
        subPath: input-$NORTHSTAR_DEPLOY_ENV
        readOnly: true
      - name: northstar-data
        mountPath: /output
        subPath: output-$NORTHSTAR_DEPLOY_ENV
        readOnly: false
      - name: northstar-data
        mountPath: /einblick_data
        subPath: einblick_data-$NORTHSTAR_DEPLOY_ENV
        readOnly: false
    resources:
      requests:
        memory: $NORTHSTAR_DAVOS_MEMORY
        cpu: $NORTHSTAR_DAVOS_CPUS

  - name: mongo
    image: mongo:4.2.3
    imagePullPolicy: Always
    ports:
      - name: mongo-1
        containerPort: 27017
      - name: mongo-2
        containerPort: 27018
      - name: mongo-3
        containerPort: 27019
    env:
      - name: MONGO_INITDB_ROOT_USERNAME
        value: $MONGODB_USER_NAME
      - name: MONGO_INITDB_ROOT_PASSWORD
        value: $MONGODB_PASSWORD
      - name: MONGO_INITDB_DATABASE
        value: admin
    volumeMounts:
      - name: northstar-data
        mountPath: /data/db
        subPath: mongodb-$NORTHSTAR_DEPLOY_ENV
        readOnly: false
    resources:
      requests:
        memory: 8Gi
        cpu: 2

  - name: grafana
    image: grafana/grafana:6.7.3
    ports:
      - name: grafana
        containerPort: 3000
    env:
      - name: GF_SECURITY_ADMIN_USER
        value: admin
      - name: GF_SECURITY_ADMIN_PASSWORD
        value: bello
    volumeMounts:
      - name: config
        mountPath: /etc/grafana/provisioning/datasources/datasources.yaml
        subPath: datasources.yaml
      - name: config
        mountPath: /etc/grafana/provisioning/dashboards/dashboards.json
        subPath: dashboards.json
      - name: config
        mountPath: /etc/grafana/provisioning/dashboards/dashboards.yaml
        subPath: dashboards.yaml
    resources:
      requests:
        memory: 4Gi
        cpu: 0.5

  - name: prometheus
    image: prom/prometheus:v2.17.2
    ports:
      - name: prometheus
        containerPort: 9090
    volumeMounts:
      - name: config
        mountPath: /etc/prometheus/prometheus.yml
        subPath: prometheus.yml
    resources:
      requests:
        memory: 4Gi
        cpu: 0.5

  - name: loki
    image: grafana/loki:v1.3.0
    ports:
      - name: loki
        containerPort: 3100
    args: ["-config.file=/etc/loki/local-config.yaml"]
    volumeMounts:
      - name: config
        mountPath: /etc/loki/local-config.yaml
        subPath: loki.yaml
    resources:
      requests:
        memory: 4Gi
        cpu: 0.5

  - name: promtail
    image: grafana/promtail:v1.3.0
    ports:
      - name: p-davos
        containerPort: 1514
      - name: p-montana
        containerPort: 1515
    args: ["-config.file=/etc/promtail/docker-config.yaml"]
    volumeMounts:
      - name: config
        mountPath: /etc/promtail/docker-config.yaml
        subPath: promtail.yaml
    resources:
      requests:
        memory: 4Gi
        cpu: 0.5

  imagePullSecrets:
  - name: $NORTHSTAR_DOCKER_SECRET

  volumes:
  - name: northstar-data
    persistentVolumeClaim:
      claimName: $NORTHSTAR_VOLUME_CLAIM
      readOnly: false
  - name: config
    configMap:
      name: config

  hostAliases:
  - ip: "127.0.0.1"
    hostnames:
      - davos
      - montana
      - grafana
      - prometheus
      - loki
      - promtail
